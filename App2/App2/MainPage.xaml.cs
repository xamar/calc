﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace App2
{
	public partial class MainPage : ContentPage
	{
		public MainPage()
		{
			InitializeComponent();
                        
            var grid = new Grid();
            Content = grid;

            for (int i = 1; i <= 7; i++)
                grid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            for (int i = 1; i <= 4; i++)
                grid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });

            //Entry value1 = new Entry{ Placeholder = "0" };
            //grid.Children.Add(value1, 0, 0);
            //Grid.SetRowSpan(value1, 2);

            //Entry opertor = new Entry { Placeholder = "+" };
            //grid.Children.Add(opertor, 1, 0);
            //Grid.SetRowSpan(opertor, 2);

            //Entry value = new Entry { Placeholder = "1" };
            //grid.Children.Add(value, 2, 0);
            //Grid.SetRowSpan(value, 2);

            //Label result = new Label();
            //grid.Children.Add(result, 3, 0);
            //Grid.SetRowSpan(result, 2);

            

            Button seven = new Button { Text = "7" };
            grid.Children.Add(seven, 0, 2);
            Button four = new Button { Text = "4" };
            grid.Children.Add(four, 0, 3);
            Button one = new Button { Text = "1" };
            grid.Children.Add(one, 0, 4);
            Button dot = new Button { Text = "DEL" };
            grid.Children.Add(dot, 0, 5);

            Button eight = new Button { Text = "8" };
            grid.Children.Add(eight, 1, 2);
            Button five = new Button { Text = "5" };
            grid.Children.Add(five, 1, 3);
            Button two = new Button { Text = "2" };
            grid.Children.Add(two, 1, 4);
            Button zero = new Button { Text = "0" };
            grid.Children.Add(zero, 1, 5);

            Button nine = new Button { Text = "9" };
            grid.Children.Add(nine, 2, 2);
            Button six = new Button { Text = "6" };
            grid.Children.Add(six, 2, 3);
            Button three = new Button { Text = "3" };
            grid.Children.Add(three, 2, 4);
            Button equalto = new Button { Text = "=" };
            grid.Children.Add(equalto, 2, 5);

            Button plus = new Button { Text = "+" };
            grid.Children.Add(plus, 3, 2);
            Button divide = new Button { Text = "/" };
            grid.Children.Add(divide, 3, 3);
            Button into = new Button { Text = "*" };
            grid.Children.Add(into, 3, 4);
            Button sub = new Button { Text = "-" };
            grid.Children.Add(sub, 3, 5);

            Button delete = new Button { Text = "DEL" };
            grid.Children.Add(delete, 0, 6);
            Button clear = new Button { Text = "CLR" };
            grid.Children.Add(clear, 1, 6);
            Button open = new Button { Text = "(" };
            grid.Children.Add(open, 2, 6);
            Button close = new Button { Text = ")" };
            grid.Children.Add(close, 3, 6);

            equalto.Clicked += Equalto_Clicked;


            //equalto.Clicked += delegate
            //{
            //    int v1 = Convert.ToInt32(value1.Text);
            //    int v2 = Convert.ToInt32(value.Text);
            //    int res;
            //    if (opertor.Text == "+")
            //    {
            //        res = v1 + v2;
            //        result.Text = res.ToString();
            //    }
            //};

        }

        private void Equalto_Clicked(object sender, EventArgs e)
        {
            
        }
    }
}
